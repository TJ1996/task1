﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Diagnostics;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.Events;

namespace MyRTSGame
{
    class GameEngine : MonoBehaviour
    {
        public const int HEIGHT = 20;
        public const int WIDTH = 20;
        private Map map1 = new Map();
        private GameObject[,] gObjects = new GameObject[20, 20];
        private List<GameObject> hpBars = new List<GameObject>();
        private float xStart = -5.187f;
        private float yStart = 2.735f;
        private Vector2 tileSize = new Vector2(0.287f, 0.287f);
        private int gameTime = 0;
        public string gameState;
        public GameObject gameOverPanel;
        public Text gameOverText;
        public Button playButton, pauseButton;
        public Sprite health100, health80, health60, health40, health20;


        void Awake()
        {
            gameOverPanel.SetActive(false);
        }

        void Start()
        {
            playButton.onClick.AddListener(play);
            pauseButton.onClick.AddListener(pause);

            //Populate maparray
            addAllTags();
            map1.generate();
            instantiateUnits();
            gameState = "running";
        }

        private float tempTime;

        void Update()
        {
            if (gameState.Equals("paused"))
            {

            }
            else
            {

                tempTime += Time.deltaTime;
                if (tempTime > 1)
                {
                    tempTime = 0;
                    updateUnits();
                    gameTime++;
                }
            }

        }

        private void play()
        {
            gameState = "running";
        }
        private void pause()
        {
            gameState = "paused";
        }

        private void updateHealthBars(Unit u)
        {
            string unitType = u.GetType().ToString();
                      
            GameObject hpBar = GameObject.FindWithTag("hp" + u.X + "," + u.Y);

            if (unitType.Contains("MeleeUnit"))
            {
                if (u.Health < 80 && u.Health > 60)
                {
                   hpBar.GetComponent<SpriteRenderer>().sprite = health80;
                }
                else if (u.Health < 60 && u.Health > 40)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health60;
                }
                else if (u.Health < 40 && u.Health > 20)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health40;
                }
                else if (u.Health < 20 && u.Health > 0)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health20;
                }
            }
            else
            {
                if (u.Health < 68 && u.Health > 51)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health80;
                }
                else if (u.Health < 51 && u.Health > 34)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health60;
                }
                else if (u.Health < 34 && u.Health > 17)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health40;
                }
                else if (u.Health < 17 && u.Health > 0)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health20;
                }
            }
        }

        private void updateHealthBars(Building b)
        {
            string buildingType = b.GetType().ToString();

            GameObject hpBar = GameObject.FindWithTag("hp" + b.X + "," + b.Y);

            if (buildingType.Contains("Corruption"))
            {
                if (b.Health < 40 && b.Health > 30)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health80;
                }
                else if (b.Health < 30 && b.Health > 20)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health60;
                }
                else if (b.Health < 20 && b.Health > 10)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health40;
                }
                else if (b.Health < 10 && b.Health > 0)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health20;
                }
            }
            else 
            {
                if (b.Health < 80 && b.Health > 60)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health80;
                }
                else if (b.Health < 60 && b.Health > 40)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health60;
                }
                else if (b.Health < 40 && b.Health > 20)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health40;
                }
                else if (b.Health < 20 && b.Health > 0)
                {
                    hpBar.GetComponent<SpriteRenderer>().sprite = health20;
                }
            }
        }

        private void instantiateUnits()
        {
            Array.Clear(gObjects, 0, gObjects.Length);

            //Draw units on map
           
            foreach (Unit u in map1.UnitsOnMap)
            {
                if (u != null)
                {
                    string unitType = u.GetType().ToString();
                    float xPos = u.X * tileSize.x + xStart;
                    float yPos = -u.Y * tileSize.y + yStart;
                    if (unitType.Contains("MeleeUnit"))
                    {
                        if (u.Faction.Equals("Red"))
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("RedMeleeUnit"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(u.X + @"," + u.Y);
                            gObject.gameObject.tag = u.X + "," + u.Y;
                            gObjects[u.X,u.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + u.X + "," + u.Y;
                        }
                        else
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("BlueMeleeUnit"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(u.X + @"," + u.Y);
                            gObject.gameObject.tag = u.X + @"," + u.Y;
                            gObjects[u.X, u.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + u.X + "," + u.Y;
                        }
                    }
                    else
                    {
                        if (u.Faction.Equals("Red"))
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("RedRangedUnit"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(u.X + @"," + u.Y);
                            gObject.gameObject.tag = u.X + "," + u.Y;
                            gObjects[u.X, u.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + u.X + "," + u.Y;
                        }
                        else
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("BlueRangedUnit"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(u.X + @"," + u.Y);
                            gObject.gameObject.tag = u.X + "," + u.Y;
                            gObjects[u.X, u.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + u.X + "," + u.Y;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine(unitType);
                }
            }

            foreach (Building b in map1.BuildingsOnMap)
            {
                if (b != null)
                {
                    string buildingType = b.GetType().ToString();
                    float xPos = b.X * tileSize.x + xStart;
                    float yPos = -b.Y * tileSize.y + yStart;
                    if (buildingType.Contains("Factory"))
                    {
                        if (b.Faction.Equals("Red"))
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("RedFactoryBuilding"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(b.X + @"," + b.Y);
                            gObject.gameObject.tag = b.X + "," + b.Y;
                            gObjects[b.X, b.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + b.X + "," + b.Y;
                        }
                        else
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("BlueFactoryBuilding"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(b.X + @"," + b.Y);
                            gObject.gameObject.tag = b.X + "," + b.Y;
                            gObjects[b.X, b.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + b.X + "," + b.Y;
                        }
                    }
                    else if (buildingType.Contains("Resource"))
                    {
                        if (b.Faction.Equals("Red"))
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("RedEnergyBuilding"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(b.X + @"," + b.Y);
                            gObject.gameObject.tag = b.X + "," + b.Y;
                            gObjects[b.X, b.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + b.X + "," + b.Y;
                        }
                        else
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("BlueEnergyBuilding"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(b.X + @"," + b.Y);
                            gObject.gameObject.tag = b.X + "," + b.Y;
                            gObjects[b.X, b.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + b.X + "," + b.Y;
                        }
                    }
                    else
                    {
                        if (b.Faction.Equals("Red"))
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("RedSpecialBuilding"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(b.X + @"," + b.Y);
                            gObject.gameObject.tag = b.X + "," + b.Y;
                            gObjects[b.X, b.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + b.X + "," + b.Y;
                        }
                        else
                        {
                            GameObject gObject = (GameObject)Instantiate(Resources.Load("BlueSpecialBuilding"), new Vector3(xPos, yPos, -2), Quaternion.identity);
                            AddTag(b.X + @"," + b.Y);
                            gObject.gameObject.tag = b.X + "," + b.Y;
                            gObjects[b.X, b.Y] = gObject;

                            GameObject hpBar = (GameObject)Instantiate(Resources.Load("100 Health"), new Vector3(gObject.transform.position.x, gObject.transform.position.y - 0.14f, -2), Quaternion.identity);
                            hpBars.Add(hpBar);
                            hpBar.tag = "hp" + b.X + "," + b.Y;
                        }
                    }
                }
            }
        }

        private void destroyUnit(int x, int y)
        {
            Destroy(GameObject.FindWithTag(x + "," + y));
            Destroy(GameObject.FindWithTag("hp" + x + "," + y));
            gObjects[x,y] = null;
        }

        private void moveUnit(int x, int y, int newX, int newY)
        {
            GameObject unit = GameObject.FindWithTag(x + "," + y);
            GameObject hpBar = GameObject.FindWithTag("hp" + x + "," + y);
            if (unit != null)
            {
                unit.gameObject.tag = newX + "," + newY;
                hpBar.gameObject.tag = "hp" + newX + "," + newY;

                float xPos = newX * tileSize.x + xStart;
                float yPos = -newY * tileSize.y + yStart;

                unit.gameObject.transform.position = new Vector3(xPos, yPos, -2f);
                hpBar.transform.position = new Vector3(xPos, unit.gameObject.transform.position.y - 0.14f, -2f);
                
                gObjects[x, y] = null;
                gObjects[newX, newY] = unit;
            }            
        } 

        public void updateUnits()
        {
            int redCount = 0, blueCount = 0;
            foreach (Unit u in map1.UnitsOnMap)
            {
                if (u.Faction.Equals("Red"))
                {
                    redCount++;
                }
                else
                {
                    blueCount++;
                }
            }
            if (blueCount == 0)
            {
                gameOverPanel.SetActive(true);
                gameOverText.text = "Red Team has won!";
            }
            else if (redCount == 0)
            {
                gameOverPanel.SetActive(true);
                gameOverText.text = "Blue Team has won!";
            }
            int blueTeamAlive = 0;
            int redTeamAlive = 0;
            List<Unit> unitsToBeRemoved = new List<Unit>();
            List<Building> buildingToBeRemoved = new List<Building>();

            foreach (Building building in map1.BuildingsOnMap)
            {
                updateHealthBars(building);
                if (building.isDestroyed())
                {
                    destroyUnit(building.X, building.Y);
                    map1.updatePos(building.X, building.Y, 999, 999, "D");
                    buildingToBeRemoved.Add(building);
                }
            }
            if (map1.BuildingsOnMap.OfType<ResourceBuilding>().Any())
            {
                foreach (ResourceBuilding rBuilding in map1.BuildingsOnMap.OfType<ResourceBuilding>())
                {
                    map1.Energy += rBuilding.tickResource();
                }
            }
            if (map1.BuildingsOnMap.OfType<FactoryBuilding>().Any())
            {
                foreach (FactoryBuilding fBuilding in map1.BuildingsOnMap.OfType<FactoryBuilding>())
                {
                    if (fBuilding.tickProduce(gameTime))
                    {
                        if (map1.MapArray[fBuilding.SpawnX, fBuilding.SpawnY].Equals(""))
                        {
                            map1.UnitsOnMap.Add(new MeleeUnit(fBuilding.UnitType, fBuilding.SpawnX, fBuilding.SpawnY, 100, 2, false, 1, fBuilding.Faction, "⚔"));
                            map1.MapArray[fBuilding.SpawnX, fBuilding.SpawnY] = "⚔";
                        }
                    }
                }
            }


            foreach (Unit unit in map1.UnitsOnMap)
            {
                if (unit != null)
                {

                
                if (!unit.isAlive())
                {
                    map1.updatePos(unit.X, unit.Y, 999, 999, "D");
                }

                else
                {
                    Unit closestUnit = unit.nearestUnit(map1.UnitsOnMap);
                    Building closestBuilding = unit.nearestBuilding(map1.BuildingsOnMap);
                    if (isUnitCloser(unit, closestUnit, closestBuilding))
                    {
                        if (unit.isWithinAttackRange(closestUnit))
                            unit.combat(closestUnit);
                        else
                        {
                            int oldX = unit.X;
                            int oldY = unit.Y;
                            map1.move(unit);
                            int newX = unit.X;
                            int newY = unit.Y;

                            moveUnit(oldX, oldY, newX, newY);
                            
                        }
                            
                    }
                    else if (!isUnitCloser(unit, closestUnit, closestBuilding))
                    {
                        if (unit.isWithinAttackRange(closestBuilding))
                            unit.combat(closestBuilding);
                        else
                        {
                            int oldX = unit.X;
                            int oldY = unit.Y;
                            map1.move(unit);
                            int newX = unit.X;
                            int newY = unit.Y;

                            moveUnit(oldX, oldY, newX, newY);
                        }
                    }
                }
                    updateHealthBars(unit);
                }
            }

            foreach (Unit unit in map1.UnitsOnMap)
            {
                if (!unit.isAlive())
                {
                    destroyUnit(unit.X, unit.Y);
                    map1.updatePos(unit.X, unit.Y, 999, 999, "D");
                    unitsToBeRemoved.Add(unit);
                }
                else
                {
                    if (unit.Faction.Equals("Red"))
                    {
                        redTeamAlive++;
                    }
                    if (unit.Faction.Equals("Blue"))
                    {
                        blueTeamAlive++;
                    }
                }

            }

            foreach (Building building in buildingToBeRemoved)
            {
                map1.BuildingsOnMap.Remove(building);
            }

            foreach (Unit unit in unitsToBeRemoved)
            {
                map1.UnitsOnMap.Remove(unit);
            }

            if (blueTeamAlive <= 0)
            {
                map1.GameState = false;
            }
            else if (redTeamAlive <= 0)
            {
                map1.GameState = false;
            }

        }

        public bool isUnitCloser(Unit currUnit, Unit u, Building b)
        {
            double buildingDistance = 0;
            double unitDistance = 0;

            if (u != null)
            {
                unitDistance = Math.Sqrt(((currUnit.X - u.X) * (currUnit.X - u.X)) + (currUnit.Y - u.Y) * (currUnit.Y - u.Y));
            }
            else
                return false;
            if (b != null)
            {
                buildingDistance = Math.Sqrt(((currUnit.X - b.X) * (currUnit.X - b.X)) + (currUnit.Y - b.Y) * (currUnit.Y - b.Y));
            }
            else
                return true;
            return unitDistance < buildingDistance;
        }

            public static void AddTag(string tag)
            {
                UnityEngine.Object[] asset = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset");
                if ((asset != null) && (asset.Length > 0))
                {
                    SerializedObject so = new SerializedObject(asset[0]);
                    SerializedProperty tags = so.FindProperty("tags");

                    for (int i = 0; i < tags.arraySize; ++i)
                    {
                        if (tags.GetArrayElementAtIndex(i).stringValue == tag)
                        {
                            return;     // Tag already present, nothing to do.
                        }
                    }

                    tags.InsertArrayElementAtIndex(0);
                    tags.GetArrayElementAtIndex(0).stringValue = tag;
                    so.ApplyModifiedProperties();
                    so.Update();
                }
            }
        
        private void addAllTags()
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    AddTag(i + "," + j);
                    AddTag("hp" + i + "," + j);
                }
            }
        }

    }
}
