﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyRTSGame
{
    class GameEngine
    {
        
        public void updateUnits(Map map1, int gameTime)
        {
            int blueTeamAlive = 0;
            int redTeamAlive = 0;
            List<Unit> unitsToBeRemoved = new List<Unit>();
            List<Building> buildingToBeRemoved = new List<Building>();

            foreach(Building building in map1.BuildingsOnMap)
            {
                if (building.isDestroyed())
                {
                    map1.updatePos(building.X, building.Y, 999, 999, "D");
                    buildingToBeRemoved.Add(building);
                }
            }
            if (map1.BuildingsOnMap.OfType<ResourceBuilding>().Any())
            {
                foreach (ResourceBuilding rBuilding in map1.BuildingsOnMap.OfType<ResourceBuilding>())
                {
                    map1.Energy += rBuilding.tickResource();
                }
            }
            if (map1.BuildingsOnMap.OfType<FactoryBuilding>().Any())
            {
                foreach (FactoryBuilding fBuilding in map1.BuildingsOnMap.OfType<FactoryBuilding>())
                {
                    if (fBuilding.tickProduce(gameTime))
                    {
                        if (map1.MapArray[fBuilding.SpawnX, fBuilding.SpawnY].Equals(""))
                        {
                            map1.UnitsOnMap.Add(new MeleeUnit(fBuilding.UnitType, fBuilding.SpawnX, fBuilding.SpawnY, 100, 2, false, 1, fBuilding.Faction, "⚔"));
                            map1.MapArray[fBuilding.SpawnX, fBuilding.SpawnY] = "⚔";
                        }
                    }
                }
            }
            foreach(Unit unit in map1.UnitsOnMap)
            {
                if (!unit.isAlive())
                {
                    map1.updatePos(unit.X, unit.Y, 999, 999, "D");
                }

                else
                {
                    Unit closestUnit = unit.nearestUnit(map1.UnitsOnMap);
                    Building closestBuilding = unit.nearestBuilding(map1.BuildingsOnMap);
                    if (isUnitCloser(unit, closestUnit, closestBuilding))
                    {
                        if (unit.isWithinAttackRange(closestUnit))
                            unit.combat(closestUnit);
                        else
                            map1.move(unit);
                    }
                    else if (!isUnitCloser(unit, closestUnit, closestBuilding))
                    {
                        if (unit.isWithinAttackRange(closestBuilding))
                            unit.combat(closestBuilding);
                        else
                            map1.move(unit);
                    }                   
                }
            }

            foreach (Unit unit in map1.UnitsOnMap)
            {
                if (!unit.isAlive())
                {
                    map1.updatePos(unit.X, unit.Y, 999, 999, "D");
                    unitsToBeRemoved.Add(unit);
                }
                else
                {
                    if (unit.Faction.Equals("Red"))
                    {
                        redTeamAlive++;
                    }
                    if (unit.Faction.Equals("Blue"))
                    {
                        blueTeamAlive++;
                    }
                }
                
            }

            foreach(Building building in buildingToBeRemoved)
            {
                map1.BuildingsOnMap.Remove(building);
            }

            foreach (Unit unit in unitsToBeRemoved)
            {
                map1.UnitsOnMap.Remove(unit);
            }

            if (blueTeamAlive <= 0)
            {
                Form1 gameEnd = new Form1();
                map1.GameState = false;
                gameEnd.gameOver("Red Team");
            }
            else if (redTeamAlive <= 0)
            {
                Form1 gameEnd = new Form1();
                map1.GameState = false;
                gameEnd.gameOver("Blue Team");
            }
            
        }

        public bool isUnitCloser(Unit currUnit, Unit u, Building b)
        {
            double buildingDistance = 0;
            double unitDistance = 0;

            if (u != null)
            {
                unitDistance = Math.Sqrt(((currUnit.X - u.X) * (currUnit.X - u.X)) + (currUnit.Y - u.Y) * (currUnit.Y - u.Y));
            }
            else
                return false;
            if (b != null)
            {
                buildingDistance = Math.Sqrt(((currUnit.X - b.X) * (currUnit.X - b.X)) + (currUnit.Y - b.Y) * (currUnit.Y - b.Y));
            }
            else
                return true;
            return unitDistance < buildingDistance;
        }
    }
}
