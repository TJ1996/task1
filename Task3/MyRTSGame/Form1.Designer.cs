﻿namespace MyRTSGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tmrGameTime = new System.Windows.Forms.Timer(this.components);
            this.btnStart = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnPause = new System.Windows.Forms.Button();
            this.lblTimeStamp = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUnitInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlFightArea = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.lblPausedDisplay = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblEnergy = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tmrGameTime
            // 
            this.tmrGameTime.Enabled = true;
            this.tmrGameTime.Interval = 1000;
            this.tmrGameTime.Tick += new System.EventHandler(this.tmrGameTime_Tick);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.MistyRose;
            this.btnStart.FlatAppearance.BorderColor = System.Drawing.Color.MistyRose;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnStart.ImageKey = "PlayButtonAsset 4ldpi.png";
            this.btnStart.ImageList = this.imageList1;
            this.btnStart.Location = new System.Drawing.Point(595, 273);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(174, 92);
            this.btnStart.TabIndex = 1;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "PauseButtonAsset 2ldpi.png");
            this.imageList1.Images.SetKeyName(1, "PausePlayButtonAsset 3ldpi.png");
            this.imageList1.Images.SetKeyName(2, "ExitButtonAsset 1ldpi.png");
            this.imageList1.Images.SetKeyName(3, "PlayButtonAsset 4ldpi.png");
            // 
            // btnPause
            // 
            this.btnPause.FlatAppearance.BorderColor = System.Drawing.Color.MistyRose;
            this.btnPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPause.ImageIndex = 0;
            this.btnPause.ImageList = this.imageList1;
            this.btnPause.Location = new System.Drawing.Point(595, 365);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(173, 89);
            this.btnPause.TabIndex = 2;
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // lblTimeStamp
            // 
            this.lblTimeStamp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTimeStamp.Location = new System.Drawing.Point(669, 35);
            this.lblTimeStamp.Name = "lblTimeStamp";
            this.lblTimeStamp.Size = new System.Drawing.Size(100, 23);
            this.lblTimeStamp.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(565, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Elapsed Time:";
            // 
            // lblUnitInfo
            // 
            this.lblUnitInfo.BackColor = System.Drawing.Color.SkyBlue;
            this.lblUnitInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnitInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUnitInfo.Location = new System.Drawing.Point(595, 100);
            this.lblUnitInfo.Name = "lblUnitInfo";
            this.lblUnitInfo.Size = new System.Drawing.Size(173, 167);
            this.lblUnitInfo.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(597, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Unit Information:";
            // 
            // pnlFightArea
            // 
            this.pnlFightArea.AutoSize = true;
            this.pnlFightArea.BackColor = System.Drawing.Color.PaleGreen;
            this.pnlFightArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlFightArea.Location = new System.Drawing.Point(26, 76);
            this.pnlFightArea.Name = "pnlFightArea";
            this.pnlFightArea.Size = new System.Drawing.Size(513, 471);
            this.pnlFightArea.TabIndex = 30;
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.MistyRose;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.ImageIndex = 2;
            this.btnExit.ImageList = this.imageList1;
            this.btnExit.Location = new System.Drawing.Point(595, 455);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(173, 90);
            this.btnExit.TabIndex = 31;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 555);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(329, 85);
            this.label3.TabIndex = 32;
            this.label3.Text = "Key:\r\n⚔    -   Melee Unit            ⚒    -   Factory Building\r\n✮    -   Ranged U" +
    "nit          ⚡    -   Resource Building\r\n                                       " +
    "⛪    -   Corruption Building\r\n  ";
            // 
            // btnLoad
            // 
            this.btnLoad.BackColor = System.Drawing.Color.OrangeRed;
            this.btnLoad.Enabled = false;
            this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoad.ImageKey = "load_39552.png";
            this.btnLoad.ImageList = this.imageList2;
            this.btnLoad.Location = new System.Drawing.Point(635, 560);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(121, 46);
            this.btnLoad.TabIndex = 33;
            this.btnLoad.Text = "Load Map";
            this.btnLoad.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLoad.UseVisualStyleBackColor = false;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "save-icon-20.png");
            this.imageList2.Images.SetKeyName(1, "load_39552.png");
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.DarkViolet;
            this.btnSave.Enabled = false;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.ImageKey = "save-icon-20.png";
            this.btnSave.ImageList = this.imageList2;
            this.btnSave.Location = new System.Drawing.Point(494, 560);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(125, 46);
            this.btnSave.TabIndex = 34;
            this.btnSave.Text = "Save Map";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblPausedDisplay
            // 
            this.lblPausedDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPausedDisplay.Location = new System.Drawing.Point(192, 21);
            this.lblPausedDisplay.Name = "lblPausedDisplay";
            this.lblPausedDisplay.Size = new System.Drawing.Size(347, 52);
            this.lblPausedDisplay.TabIndex = 0;
            this.lblPausedDisplay.Text = "PAUSED";
            this.lblPausedDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblPausedDisplay.Visible = false;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(35, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 23);
            this.label4.TabIndex = 35;
            this.label4.Text = "Energy: ";
            // 
            // lblEnergy
            // 
            this.lblEnergy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEnergy.Location = new System.Drawing.Point(97, 35);
            this.lblEnergy.Name = "lblEnergy";
            this.lblEnergy.Size = new System.Drawing.Size(100, 23);
            this.lblEnergy.TabIndex = 36;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(808, 636);
            this.Controls.Add(this.lblEnergy);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblPausedDisplay);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlFightArea);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblUnitInfo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTimeStamp);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Combat Simulation RTS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrGameTime;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Label lblTimeStamp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUnitInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlFightArea;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblPausedDisplay;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblEnergy;
    }
}

