﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRTSGame
{
    class Map
    {
        private string[,] mapArray = new string[20, 20];
        private List<Unit> unitsOnMap = new List<Unit>();
        private List<Building> buildingsOnMap = new List<Building>();

        private int meleeAmount;
        private int rangedAmount;
        private bool gameStateActive = false;
        private int blueAmount;
        private int redAmount;

        public bool GameState
        {
            get { return gameStateActive; }
            set { gameStateActive = value; }
        }
        public string[,] MapArray
        {
            get { return mapArray; }
            set { mapArray = value; }
        }

        public int MeleeAmount
        {
            get { return meleeAmount; }
            set { meleeAmount = value; }
        }
        public int RangedAmount
        {
            get { return rangedAmount; }
            set { rangedAmount = value; }
        }
        public int BlueAmount
        {
            get { return blueAmount; }
            set { blueAmount = value; }
        }
        public int RedAmount
        {
            get { return redAmount; }
            set { redAmount = value; }
        }

        public List<Unit> UnitsOnMap
        {
            get { return unitsOnMap; }
            set { unitsOnMap = value; }
        }

        public List<Building> BuildingsOnMap
        {
            get { return buildingsOnMap; }
            set { buildingsOnMap = value; }
        }

        public void generate()
        {
            Random rndNum = new Random();

            int xPosition = 0;
            int yPosition = 0;
            string teamColor = "";
            meleeAmount = rndNum.Next(5, 20);
            rangedAmount = rndNum.Next(5, 20);

            unitsOnMap.Clear();

            for (int row = 0; row < 20; row++)
            {
                for (int col = 0; col < 20; col++)
                {
                    mapArray[col, row] = "";
                }
            }

            int blueTeam = rndNum.Next(1, meleeAmount - 2);
            blueAmount = blueTeam;
            redAmount = meleeAmount - blueTeam;

            for (int i = 0; i < meleeAmount; i++)
            {
                do {
                    xPosition = rndNum.Next(0, 20);
                    yPosition = rndNum.Next(0, 20);
                } while (!mapArray[xPosition, yPosition].Equals(""));

                teamColor = (blueTeam > 0) ? "Blue" : "Red";
                blueTeam--;

                //listOfMeleeUnits.Add(new MeleeUnit(xPosition, yPosition, 100, 2, 6, 1, teamColor, "⚔"));
                unitsOnMap.Add(new MeleeUnit("Infantry", xPosition, yPosition, 100, 2, false, 1, teamColor, "⚔"));
                mapArray[xPosition, yPosition] = "⚔";

                //if (xPosition == arrayOfMeleeUnits[i].XPos && yPosition == arrayOfMeleeUnits[i].YPos)
                //{

                //}

            }

            blueTeam = rndNum.Next(1, rangedAmount - 2);
            blueAmount += blueTeam;
            redAmount += rangedAmount - blueTeam;

            for (int i = 0; i < rangedAmount; i++)
            {
                do
                {
                    xPosition = rndNum.Next(0, 20);
                    yPosition = rndNum.Next(0, 20);
                } while (!mapArray[xPosition, yPosition].Equals(""));

                teamColor = (blueTeam > 0) ? "Blue" : "Red";
                blueTeam--;

                //listOfRangedUnits.Add(new RangedUnit(xPosition, yPosition, 85, 4, 4, 3, teamColor, "✮"));
                unitsOnMap.Add(new RangedUnit("Ranger", xPosition, yPosition, 85, 4, false, 3, teamColor, "✮"));

                mapArray[xPosition, yPosition] = "✮";
            }
            buildingsOnMap.Clear();
            for (int i = 0; i < 4; i++)
            {
                do
                {
                    xPosition = rndNum.Next(0, 20);
                    yPosition = rndNum.Next(0, 20);
                } while (!mapArray[xPosition, yPosition].Equals(""));

                if (i == 0)
                {
                    buildingsOnMap.Add(new FactoryBuilding(xPosition, yPosition, 100, "Red", "⚒", "Melee", 10, 1, 1));
                    mapArray[xPosition, yPosition] = "⚒";
                }
                else if (i == 1)
                { 
                    buildingsOnMap.Add(new FactoryBuilding(xPosition, yPosition, 100, "Blue", "⚒", "Melee", 10, 1, 1));
                    mapArray[xPosition, yPosition] = "⚒";
                }
                else if (i == 2)
                { 
                    buildingsOnMap.Add(new ResourceBuilding(xPosition, yPosition, 100, "Red", "⚡", "Energy", 5, 200));
                    mapArray[xPosition, yPosition] = "⚡";
                }
                else if (i == 3)
                {
                    buildingsOnMap.Add(new ResourceBuilding(xPosition, yPosition, 100, "Blue", "⚡", "Energy", 5, 200));
                    mapArray[xPosition, yPosition] = "⚡";
                }
            }
        }

        public void move(Unit currentUnit)
        {
            Unit closestEnemy = null;
            string direction = "";
            int oldX = currentUnit.X;
            int oldY = currentUnit.Y;

            closestEnemy = currentUnit.nearestUnit(unitsOnMap);

            if (closestEnemy.Y > currentUnit.Y)
                direction += "D";

            else if (closestEnemy.Y < currentUnit.Y)
                direction += "U";

            if (closestEnemy.X > currentUnit.X)
                direction += "R";

            else if (closestEnemy.X < currentUnit.X)
                direction += "L";

            if (currentUnit.move(mapArray, direction))
            {
                updatePos(oldX, oldY, currentUnit.X, currentUnit.Y, currentUnit.Symbol);
            }     
        }

        public void updatePos(int oldX, int oldY, int newX, int newY, string symbol)
        {
            if (newX == 999)
                mapArray[oldX, oldY] = "";

            else
            { 
                mapArray[newX, newY] = symbol;
                mapArray[oldX, oldY] = ""; 
            }
        }

        public int getIndexOf(int x, int y, string type)
        {
            if (type.Equals("unit"))
            {
                foreach (Unit unit in unitsOnMap)
                {
                    if (unit.X == x && unit.Y == y)
                    {
                        return unitsOnMap.IndexOf(unit);
                    }
                }
            }
            else
            {
                foreach (Building building in buildingsOnMap)
                {
                    if (building.X == x && building.Y == y)
                    {
                        return buildingsOnMap.IndexOf(building);
                    }
                }
            }

            return -1;
        }

        public void newMapLoad()
        {
            for (int col = 0; col < 20; col++)
            {
                for (int row = 0; row < 20; row++)
                {
                    mapArray[row, col] = "";
                    foreach (Unit unit in unitsOnMap)
                    {
                        if (unit.X == row && unit.Y == col)
                        {
                            mapArray[row, col] = unit.Symbol;
                        }
                    }
                    if (mapArray[row, col] == "")
                    {
                        foreach (Building building in buildingsOnMap)
                        {
                            if (building.X == row && building.Y == col)
                            {
                                mapArray[row, col] = building.Symbol;
                            }
                        }
                    }
                }
            }
        }

    }
}
