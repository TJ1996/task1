*GADE 6112
Theunus Voges 17605849
VEGA School Cape Town POE Assignment

Task 1:

Creating the basics of our final program, the class hierarchy, displaying our knowledge of polymorphism and inheritance and some form UI design through the initial set up of our game which will change later on.

Task 2:

Implementing the Save and Load to our game as well as additional buildings and properties for the units.

Task 3:

Creating our Repository, adding additional unique features to our game and writing out a high concept document for our game.

FINAL POE:

Transfer our game to UNITY.