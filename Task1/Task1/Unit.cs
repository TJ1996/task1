﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    public class Unit
    {
        private int xPos;
        private int yPos;
        private int health;
        private int speed;
        private int attack;
        private int attackRange;
        private string team;
        private string symbol;

        #region Accessors
        public int XPos
        {
            get { return xPos; }
            set { xPos = value; }
        }

        public int YPos
        {
            get { return yPos; }
            set { yPos = value; }
        }

        public int Health
        {
            get { return health; }
            set { health = value; }            
        }

        public int Speed
        {
            get { return speed; }
            set { speed = value; }
            
        }

        public int Attack
        {
            get { return attack; }
            set { attack = value; }
            
        }

        public int AttackRange
        {
            get { return attackRange; }
            set { attackRange = value; }
            
        }

        public string Team
        {
            get { return team; }
            set { team = value; }
            
        }

        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
            
        }
        #endregion

        public void move(int xPosNew, int yPosNew)
        {
            this.xPos = xPosNew;
            this.yPos = yPosNew;
        }

        public void combat(int attack)
        {
            health -= attack;
        }

        public bool isInRange(int enemyXPos, int enemyYPos)
        {
            int distance;
            int a = enemyXPos - this.xPos;
            int b = enemyYPos - this.yPos;
            distance = (a * a) + (b * b);
            distance = (int)Math.Sqrt(distance);

            if (distance <= this.attackRange)
                return true;

            else
                return false;
        }

        public int unitDistance(int secondUnitPosX, int secondUnitPosY)
        {
            int distance;
            int a = secondUnitPosX - this.xPos;
            int b = secondUnitPosY - this.yPos;
            distance = (a * a) + (b * b);
            distance = (int)Math.Sqrt(distance);

            return distance;
        }

        public bool isDead()
        {
            if (health < 0)
            {
                return true;
            }
        }



        public string toString()
        {
            string output = "";
            output = "Position: " + xPos + " , " + yPos + "\n";
            output += "Health: " + health + "\n";
            output += "Speed: " + speed + "\n";
            output += "Attack: " + attack + "\n";
            output += "Attack Range: " + attackRange + "\n";
            output += "Team: " + team + "\n";
            output += "Symbol: " + symbol + "\n";
            return output;
        }
    }

}
