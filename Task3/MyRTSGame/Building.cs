﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MyRTSGame
{
    abstract class Building
    {
        protected int x;
        protected int y;
        protected int health;
        protected string faction;
        protected string symbol;
        #region Constructor
        public Building(int x, int y, int health, string faction, string symbol)
        {
            this.x = x;
            this.y = y;
            this.health = health;
            this.faction = faction;
            this.symbol = symbol;
        }
        #endregion

        //destructor
        ~Building()
        {
        }

        #region Accessor
        public int X
        {
            get { return this.x; }
            set {this.x = value; }
        }

        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public int Health
        {
            get { return this.health; }
            set { this.health = value; }
        }

        public string Faction
        {
            get { return this.faction; }
            set { this.faction = value; }
        }

        public string Symbol
        {
            get { return this.symbol; }
            set { this.symbol = value; }
        }
        #endregion
        public abstract bool isDestroyed();
        public abstract void saveToFile(string fileName);
        public abstract void loadFromFile(string buildingDetails);
        public abstract string toString();
    }    
}
