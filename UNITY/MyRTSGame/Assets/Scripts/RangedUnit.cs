﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;

namespace MyRTSGame
{
    class RangedUnit : Unit
    {
        #region Constructor
        private const int DAMAGE = 4;
        private const int MOVEDISTANCE = 4;
        private const int MAX_HEALTH = 100;

        public RangedUnit(string name,int x, int y, int health, int speed, bool attack, int attackRange, string faction, string symbol)
            : base(name, x, y, health, speed, attack, attackRange, faction, symbol)
        {
        }

        #endregion


        public override bool move(string[,] mapArray, string direction)
        {
            int moveDistance = MOVEDISTANCE;

            int modX = this.X;
            int modY = this.Y;

            for (int i = moveDistance; i > 0; i--)
            {
                //if (Health > 25)
                //{
                    switch (direction)
                    {
                        case "UL":
                            modY -= i;
                            modX -= i;
                            break;
                        case "L":
                            modX -= i;
                            break;
                        case "DL":
                            modY -= i;
                            modX -= i;
                            break;
                        case "D":
                            modY += i;
                            break;
                        case "DR":
                            modY += i;
                            modX += i;
                            break;
                        case "R":
                            modX += i;
                            break;
                        case "UR":
                            modY -= i;
                            modX += i;
                            break;
                        case "U":
                            modY -= i;
                            break;
                        default:
                            break;
                    }
                /*}
                else
                {
                    switch (direction)
                    {
                        case "DR":
                            modY -= i;
                            modX -= i;
                            break;
                        case "R":
                            modX -= i;
                            break;
                        case "UR":
                            modY -= i;
                            modX -= i;
                            break;
                        case "U":
                            modY += i;
                            break;
                        case "UL":
                            modY += i;
                            modX += i;
                            break;
                        case "L":
                            modX += i;
                            break;
                        case "DL":
                            modY -= i;
                            modX += i;
                            break;
                        case "D":
                            modY -= i;
                            break;
                        default:
                            break;
                    }
                }*/

                if (modX < 0 || modY < 0 || modX > 19 || modY > 19)
                {
                    if (modX < 0)
                    {
                        modX = 0;
                    }
                    if (modY < 0)
                    {
                        modY = 0;
                    }
                    if (modX > 19)
                    {
                        modX = 19;
                    }
                    if (modY > 19)
                    {
                        modY = 19;
                    }
                }

                if (mapArray[modX, modY].Equals(""))
                {
                    this.X = modX;
                    this.Y = modY;
                    return true;
                }

                if (modX < 19)
                {
                    if (mapArray[modX + 1, modY].Equals(""))
                    {
                        this.X = (modX + 1);
                        this.Y = modY;
                        return true;
                    }
                }
                if (modY < 19)
                {
                    if (mapArray[modX, modY + 1].Equals(""))
                    {
                        this.X = modX;
                        this.Y = (modY + 1);
                        return true;
                    }
                }
                if (modX > 0)
                {
                    if (mapArray[modX - 1, modY].Equals(""))
                    {
                        this.X = (modX - 1);
                        this.Y = modY;
                        return true;
                    }
                }
                if (modY > 0)
                {
                    if (mapArray[modX, modY - 1].Equals(""))
                    {
                        this.X = modX;
                        this.Y = (modY - 1);
                        return true;
                    }
                }
                if (modX > 0 && modY > 0)
                {
                    if (mapArray[modX - 1, modY - 1].Equals(""))
                    {
                        this.X = (modX - 1);
                        this.Y = (modY - 1);
                        return true;
                    }
                }
                if (modX < 19 && modY < 19)
                {
                    if (mapArray[modX + 1, modY + 1].Equals(""))
                    {
                        this.X = (modX + 1);
                        this.Y = (modY + 1);
                        return true;
                    }
                }
            }
            return false;
        }

        public override void combat(Unit enemy)
        {
            // if (this.Health < MAX_HEALTH * 0.25)
            //     this.Attack = false;

            // else
            // {
            if (enemy != null)
            {
                enemy.Health -= DAMAGE;
            }
            //if (enemy.Health > 0)
            //    this.Attack = true;
            //}
        }

        public override void combat(Building enemyBuilding)
        {
            if (enemyBuilding != null)
            {
                enemyBuilding.Health -= DAMAGE;
            }
        }

        public override bool isWithinAttackRange(Unit enemy)
        {
            if (enemy != null)
            {
                int xDif = Math.Abs(enemy.X - this.X);
                int yDif = Math.Abs(enemy.Y - this.Y);

                if (xDif < 4 && yDif < 4)
                    return true;
                //else
                //{
                //    this.Attack = false;
                else
                    return false;
            }
            else
            {
                return false;
            }
            
        }

        public override bool isWithinAttackRange(Building enemyBuilding)
        {
            if (enemyBuilding != null)
            {
                int xDif = Math.Abs(enemyBuilding.X - this.X);
                int yDif = Math.Abs(enemyBuilding.Y - this.Y);

                if (xDif < 4 && yDif < 4)
                    return true;

                //else
                //{
                //    this.Attack = false;
                else
                    return false;
            }
            else
                return false;
            //}                  
        }

        public override Unit nearestUnit(List<Unit> uList)
        {
            Unit closestUnit = null;
            double closestRange = 2000;
            double distance = 0;
            int xRange, yRange;

            foreach (Unit unit in uList)
            {
                if (!unit.Faction.Equals(this.Faction))
                {
                    xRange = this.X - unit.X;
                    yRange = this.Y - unit.Y;

                    distance = Math.Sqrt(Math.Pow(xRange, 2) + Math.Pow(yRange, 2));

                    if (distance < closestRange)
                    {
                        closestRange = distance;
                        closestUnit = unit;
                    }
                }
            }
            return closestUnit;
        }

        public override Building nearestBuilding(List<Building> bList)
        {
            Building closestBuilding = null;
            double closestRange = 2000;
            double distance = 0;
            int xRange, yRange;

            foreach (Building building in bList)
            {
                if (!building.Faction.Equals(this.Faction))
                {
                    xRange = this.X - building.X;
                    yRange = this.Y - building.Y;

                    distance = Math.Sqrt(Math.Pow(xRange, 2) + Math.Pow(yRange, 2));

                    if (distance < closestRange)
                    {
                        closestRange = distance;
                        closestBuilding = building;
                    }
                }
            }
            return closestBuilding;
        }

        public override bool isAlive()
        {
            return (Health > 0);            
        }

        public override void saveToFile(string fileName)
        {
            FileStream fileOut = null;
            StreamWriter writer = null;

            try
            {
                fileOut = new FileStream(@"Files\" + fileName, FileMode.Append, FileAccess.Write);
                writer = new StreamWriter(fileOut);

                writer.WriteLine(Name + "," + X + "," + Y + "," + Health + "," + Speed + "," + Attack + "," + AttackRange + "," + Faction + "," + Symbol);

                writer.Close();
                fileOut.Close();
            }
            catch (Exception fe)
            {
                Debug.WriteLine(fe.Message);
            }
        }

        public override void loadFromFile(string unitDetails)
        {
            string[] detailsArray = new string[9];
            detailsArray = unitDetails.Split(',');

            this.Name = detailsArray[0];
            this.X = int.Parse(detailsArray[1]);
            this.Y = int.Parse(detailsArray[2]);
            this.Health = int.Parse(detailsArray[3]);
            this.Speed = int.Parse(detailsArray[4]);
            this.Attack = bool.Parse(detailsArray[5]);
            this.AttackRange = int.Parse(detailsArray[6]);
            this.Faction = detailsArray[7];
            this.Symbol = detailsArray[8];
        }

        public override string toString()
        {
            string output = "Name: " + Name + "\n";
            output += "X Co-ordinate: " + X + "\n";
            output += "Y Co-ordinate: " + Y + "\n";
            output += "Health: " + Health + "\n";
            output += "Speed: " + Speed + "\n";
            output += "Attacking: " + (Attack ? "Yes" : "No") + "\n";
            output += "Attack Range: " + AttackRange + "\n";
            output += "Faction: " + Faction + "\n";
            output += "Symbol: " + Symbol + "\n";

            return output;
        }            
    }
}
