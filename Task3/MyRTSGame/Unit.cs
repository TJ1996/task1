﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRTSGame
{
    abstract class Unit
    {
        private string name;
        private int x;
        private int y;
        private int health;
        private int speed;
        private bool attack;
        private int attackRange;
        private string faction;
        private string symbol;

        #region Constructor

        public Unit(string name,int x, int y, int health, int speed, bool attack, int attackRange, string faction, string symbol)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            this.health = health;
            this.speed = speed;
            this.attack = attack;
            this.attackRange = attackRange;
            this.faction = faction;
            this.symbol = symbol;
        }
        #endregion

        //destructor
        ~Unit()
        {
        }

        #region Accessors
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public int Health
        {
            get { return health; }
            set { health = value; }
        }
        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }
        public bool Attack
        {
            get { return attack; }
            set { attack = value; }
        }
        public int AttackRange
        {
            get { return attackRange; }
            set { attackRange = value; }
        }
        public string Faction
        {
            get { return faction; }
            set { faction = value; }
        }
        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }
        #endregion

        public abstract bool move(string[,] mapArray, string direction);
        public abstract void combat(Unit enemy);
        public abstract void combat(Building enemyBuilding);
        public abstract bool isWithinAttackRange(Unit enemy);
        public abstract bool isWithinAttackRange(Building enemyBuilding);
        public abstract Unit nearestUnit(List<Unit> list);
        public abstract Building nearestBuilding(List<Building> bList);
        public abstract bool isAlive();
        public abstract void saveToFile(string fileName);
        public abstract void loadFromFile(string unitDetails);
        public abstract string toString();
        

    }
}
