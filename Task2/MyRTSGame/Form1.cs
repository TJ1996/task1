﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MyRTSGame
{
    public partial class Form1 : Form
    {
        private int gameTime = 0;
        private Label[,] labelArr = new Label[20, 20];
        private Map map1 = new Map();

        public Form1()
        {
            InitializeComponent();
        }

        private void form_load(object sender, EventArgs e)
        {
            tmrGameTime.Interval = 1000;
            tmrGameTime.Start();       
        }
     
        public void tmrGameTime_Tick(object sender, EventArgs e)
        {
            //Map update1 = new Map();
            //update1.updatePos();
            //if (gameTime % 3 == 0)
            //{
            //    printLabels();
            //}    
            if (map1.GameState)
            {
                gameTime++;
                lblTimeStamp.Text = Convert.ToString(gameTime) + " s";

                GameEngine gameEngine = new GameEngine();
                gameEngine.updateUnits(map1);

                updateLabels();                
            }   

            
        }

        private void labelMouseUp(Object sender, EventArgs e)
        {
            Label clickedLabel = (Label)sender;
            int unitClickedIndex = -1;
            string typeClicked = "unit";
            indexOfLabel(clickedLabel, ref unitClickedIndex, ref typeClicked);
            string output = "";
            
            if (unitClickedIndex != -1)
            {
                if (typeClicked.Equals("unit"))
                    output = map1.UnitsOnMap[unitClickedIndex].toString();

                else
                    output = map1.BuildingsOnMap[unitClickedIndex].ToString();
            }
            
            lblUnitInfo.Text = output;
        }

        private void indexOfLabel(Label labelToFind, ref int unitClickedIndex, ref string typeClicked)
        {
            
            for (int row = 0; row < 20; row++)
            {
                for (int col = 0; col < 20; col++)
                {
                    if (labelArr[col, row].Name == labelToFind.Name)
                    {
                        if (!labelToFind.Text.Equals(""))
                        {
                            if (labelToFind.Text.Equals("✮") || labelToFind.Text.Equals("⚔"))
                            {
                                unitClickedIndex = map1.getIndexOf(col, row, "unit");
                                typeClicked = "unit";
                            }
                            else
                            {
                                unitClickedIndex = map1.getIndexOf(col, row, "building");
                                typeClicked = "building";
                            }
                        }                                                    
                    }
                }
            }
        }
        private void printLabels()
        {
            pnlFightArea.Controls.Clear();
            int labelWidth = 19;
            int labelHeight = 19;
            int labelXPos = 0;
            int labelYPos = 0;
            pnlFightArea.Visible = false;

            for (int row = 0; row < 20; row++)
            {
                for (int col = 0; col < 20; col++)
                {
                    Label lblMapTile = new Label();
                    lblMapTile.Name = "lblR" + row + "C" + col;
                    lblMapTile.Location = new Point(labelXPos, labelYPos);
                    lblMapTile.Text = map1.MapArray[col, row];

                    if (!lblMapTile.Text.Equals(""))
                    {
                        if (lblMapTile.Text.Equals("✮") || lblMapTile.Text.Equals("⚔"))
                        {
                            if (map1.UnitsOnMap[map1.getIndexOf(col, row, "unit")].Faction.Equals("Red"))
                                lblMapTile.ForeColor = Color.Red;

                            else
                                lblMapTile.ForeColor = Color.Blue;
                        }
                        else
                        {
                            if (map1.BuildingsOnMap[map1.getIndexOf(col, row, "building")].Faction.Equals("Red"))
                                lblMapTile.ForeColor = Color.Red;

                            else
                                lblMapTile.ForeColor = Color.Blue;
                        }
                    }
                    
                    lblMapTile.Size = new Size(labelWidth, labelHeight);
                    lblMapTile.Visible = true;
                    lblMapTile.BorderStyle = BorderStyle.FixedSingle;
                    lblMapTile.Margin = new Padding(0, 0, 0, 0);

                    pnlFightArea.Controls.Add(lblMapTile);
                    lblMapTile.MouseUp += new MouseEventHandler(this.labelMouseUp);

                    labelArr[col, row] = lblMapTile;
                    labelXPos += 19;
                }
                labelYPos += 19;
                labelXPos = 0;
            }
            pnlFightArea.Visible = true;
        }
        public void updateLabels()
        {
            for (int row = 0; row < 20; row++)
            {
                for (int col = 0; col < 20; col++)
                {
                    labelArr[col, row].Text = map1.MapArray[col, row];

                    if (!labelArr[col, row].Text.Equals(""))
                    {
                        if (labelArr[col, row].Text.Equals("✮") || labelArr[col, row].Text.Equals("⚔"))
                        {
                            if (map1.UnitsOnMap[map1.getIndexOf(col, row, "unit")].Faction.Equals("Red"))
                                labelArr[col, row].ForeColor = Color.Red;

                            else
                                labelArr[col, row].ForeColor = Color.Blue;
                        }
                        else if (labelArr[col, row].Text.Equals("⚒") || labelArr[col, row].Text.Equals("⚡"))
                        {
                            if (map1.BuildingsOnMap[map1.getIndexOf(col, row, "building")].Faction.Equals("Red"))
                                labelArr[col, row].ForeColor = Color.Red;

                            else
                                labelArr[col, row].ForeColor = Color.Blue;
                        }

                    }
                }
            }

        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (map1.GameState)
            {
                DialogResult exitBox = MessageBox.Show("Are you sure you wish to start a new game?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (exitBox == DialogResult.Yes)
                {
                    map1.generate();
                    printLabels();
                    map1.GameState = true;
                    btnLoad.Enabled = true;
                    btnSave.Enabled = true;
                }
            }
            else
            {
                map1.generate();
                printLabels();
                map1.GameState = true;
                btnLoad.Enabled = true;
                btnSave.Enabled = true;
            }
            
            //btnStart.Enabled = false;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            if (btnPause.ImageIndex == 0)
            {
                lblPausedDisplay.Visible = true;
                lblPausedDisplay.BringToFront();
                map1.GameState = false;
                btnPause.ImageIndex = 1;
            }
            else 
            {
                lblPausedDisplay.Visible = false;
                map1.GameState = true;
                btnPause.ImageIndex = 0;
            }
            
        }
       
        public void gameOver(string winningTeam)
        {            
            MessageBox.Show("The " + winningTeam + " has won!", "Congratulations " + winningTeam, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            map1.GameState = false;
            DialogResult exitBox = MessageBox.Show("Are you sure you wish to exit?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (exitBox == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                btnPause_Click(null, e);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            btnPause_Click(null, e);
            DialogResult dr = MessageBox.Show("Are you sure you would like to load an old game? That will result in losing all current process.", "Load Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            
            if (dr == DialogResult.Yes)
            {
                


                string directoryName = @"Files\";
                string[] listOfFiles;
                List<Unit> newUnitList = new List<Unit>();
                List<Building> newBuildingList = new List<Building>();

                listOfFiles = Directory.GetFiles(directoryName);

                FileListDialog listDialog = new FileListDialog(listOfFiles);
                listDialog.ShowDialog();

                FileStream inFile = new FileStream(listDialog.SelectedFile, FileMode.Open, FileAccess.Read);
                StreamReader reader = new StreamReader(inFile);

                string nextLine = reader.ReadLine();
                while (nextLine != "Buildings:")
                {
                    if (nextLine != "Units: ")
                    {
                        if (nextLine.Contains("⚔"))
                        {
                            MeleeUnit mu = new MeleeUnit("", 0, 0, 0, 0, false, 0, "", "");
                            mu.loadFromFile(nextLine);
                            newUnitList.Add(mu);
                        }
                        if (nextLine.Contains("✮"))
                        {
                            RangedUnit ru = new RangedUnit("", 0, 0, 0, 0, false, 0, "", "");
                            ru.loadFromFile(nextLine);
                            newUnitList.Add(ru);
                        }

                    }
                    nextLine = reader.ReadLine();
                }
                while (nextLine != null)
                {

                    if (nextLine != "Buildings: ")
                    {
                        if (nextLine.Contains("⚒"))
                        {
                            FactoryBuilding fb = new FactoryBuilding(0, 0, 0, "", "", "", 0, 0, 0);
                            fb.loadFromFile(nextLine);
                            newBuildingList.Add(fb);
                        }
                        if (nextLine.Contains("⚡"))
                        {
                            ResourceBuilding rb = new ResourceBuilding(0, 0, 0, "", "", "", 0, 0);
                            rb.loadFromFile(nextLine);
                            newBuildingList.Add(rb);
                        }
                    }
                    nextLine = reader.ReadLine();
                }
                map1.UnitsOnMap = newUnitList;
                map1.BuildingsOnMap = newBuildingList;
                map1.newMapLoad();
                updateLabels();
                btnPause_Click(null, e);
            }
            else
                btnPause_Click(null, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string currDate = DateTime.Now + "-Save.txt";
            string fileName = "";
            char[] fileNameArray = new char[currDate.Length];
            for (int i = 0; i < currDate.Length; i++)
            {
                if (currDate[i].Equals('/') || currDate[i].Equals(':'))
                {
                    fileNameArray[i] = '.';
                }
                else
                {
                    fileNameArray[i] = currDate[i];
                }

                fileName += fileNameArray[i];
            }

            btnPause_Click(null, e);

            FileStream outFile = new FileStream(@"Files\" + fileName, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(outFile);


            writer.WriteLine("Units:");

            writer.Close();
            outFile.Close();

            foreach (Unit currUnit in map1.UnitsOnMap)
            {
                currUnit.saveToFile(fileName);
            }

            outFile = new FileStream(@"Files\" + fileName, FileMode.Append, FileAccess.Write);
            writer = new StreamWriter(outFile);

            writer.WriteLine("Buildings:");

            writer.Close();
            outFile.Close();

            foreach (Building currBuilding in map1.BuildingsOnMap)
            {
                currBuilding.saveToFile(fileName);
            }

            writer.Close();
            outFile.Close();

            MessageBox.Show("Your game has been saved to file: " + fileName, "Great Save!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnPause_Click(null, e);
        }
    }
}
