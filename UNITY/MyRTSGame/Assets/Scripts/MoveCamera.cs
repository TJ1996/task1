﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    [SerializeField]
    private float cameraSpeed = 50;
    [SerializeField]
    private Vector3 minCameraPoint, maxCameraPoint;

    public void Update()
    {
        keyEventInput();
    }

    private void keyEventInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            transform.Translate(Vector3.up * cameraSpeed * Time.deltaTime*2);   
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            transform.Translate(Vector3.left * cameraSpeed * Time.deltaTime * 2);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            transform.Translate(Vector3.down * cameraSpeed * Time.deltaTime * 2);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.Translate(Vector3.right * cameraSpeed * Time.deltaTime * 2);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            Camera.main.orthographicSize += 1.5f;
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Camera.main.orthographicSize -= 1.5f;
        }
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minCameraPoint.x, maxCameraPoint.x), Mathf.Clamp(transform.position.y, minCameraPoint.y, maxCameraPoint.y),  -10);
    }   
}
