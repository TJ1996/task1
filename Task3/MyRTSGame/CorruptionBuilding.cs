﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace MyRTSGame
{
    class CorruptionBuilding : Building
    {
        protected int remainingUses;
        protected int energyCost;

        #region Constructor
        public CorruptionBuilding(int x, int y, int health, string faction, string symbol, int energyCost, int remainingUses)
            : base(x, y, health, faction, symbol)
        {
            this.energyCost = energyCost;
            this.remainingUses = remainingUses;
        }
        #endregion
        #region Accessors
        public int RemainingUses
        {
            get { return this.remainingUses; }
            set { this.remainingUses = value; }
        }
        public int EnergyCost
        {
            get { return this.energyCost; }
            set { this.energyCost = value; }
        }
        #endregion

        public bool corrupt(List<Unit> unitList)
        {
            if (remainingUses > 0 )
            {
                remainingUses -= 1;
                foreach (Unit u in unitList)
                {
                    if (Math.Abs(u.X - this.x) < 2 && Math.Abs(u.Y - this.y) < 2 && !u.Faction.Equals(this.faction))
                    {
                        u.Faction = this.faction;
                    }
                }
                return true;
            }
            else 
            {
                return false;
            }
        }

        public override bool isDestroyed()
        {
            return (health <= 0);
        }

        public override void saveToFile(string fileName)
        {
            FileStream fileOut = null;
            StreamWriter writer = null;

            try
            {
                fileOut = new FileStream(@"Files\" + fileName, FileMode.Append, FileAccess.Write);
                writer = new StreamWriter(fileOut);

                writer.WriteLine(X + "," + Y + "," + Health + "," + Faction + "," + Symbol + "," + remainingUses + ", " + energyCost);

                writer.Close();
                fileOut.Close();
            }
            catch (Exception fe)
            {
                Debug.WriteLine(fe.Message);
            }
        }
        public override void loadFromFile(string resourceDetails)
        {
            string[] detailsArray = new string[6];
            detailsArray = resourceDetails.Split(',');

            this.x = int.Parse(detailsArray[0]);
            this.y = int.Parse(detailsArray[1]);
            this.health = int.Parse(detailsArray[2]);
            this.faction = detailsArray[3];
            this.symbol = detailsArray[4];
            this.remainingUses = int.Parse(detailsArray[5]);
            this.energyCost = int.Parse(detailsArray[6]);
        }

        public override string toString()
        {
            string output = "X Co-ordinate: " + X + "\n";
            output += "Y Co-ordinate: " + Y + "\n";
            output += "Health: " + Health + "\n";
            output += "Faction: " + Faction + "\n";
            output += "Symbol: " + Symbol + "\n";
            output += "Uses Remaining: " + remainingUses + "\n";
            output += "Energy Cost: " + energyCost;

            return output;
        }
    }
}
