﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace MyRTSGame
{
    class ResourceBuilding : Building
    {
        protected string resourceType;
        protected int resourcesPerTick;
        protected int resourcesRemaining;
        #region Constructor
        public ResourceBuilding(int x, int y, int health, string faction, string symbol, string resourceType, int resourcesPerTick, int resourcesRemaining)
            : base(x,y,health,faction,symbol)
        {
            this.resourceType = resourceType;
            this.resourcesPerTick = resourcesPerTick;
            this.resourcesRemaining = resourcesRemaining;
        }
        #endregion
        #region Accessors
        public string ResourceType
        {
            get { return this.resourceType; }
            set { this.resourceType = value; }
        }
        public int ResourcesPerTick
        {
            get { return this.resourcesPerTick; }
            set { this.resourcesPerTick = value; }
        }
        public int ResourcesRemaining
        {
            get { return this.resourcesRemaining; }
            set { this.resourcesRemaining = value; }
        }
        #endregion

        public int tickResource()
        {
            if (resourcesRemaining <= 0)
                return 0;
            else
            {
                resourcesRemaining -= resourcesPerTick;
                return resourcesPerTick;
            }
        }

        public override bool isDestroyed()
        {
            return (health <= 0);
        }
        public override void saveToFile(string fileName)
        {
            FileStream fileOut = null;
            StreamWriter writer = null;

            try
            {
                fileOut = new FileStream(@"Files\" + fileName, FileMode.Append, FileAccess.Write);
                writer = new StreamWriter(fileOut);

                writer.WriteLine(X + "," + Y + "," + Health + "," + Faction + "," + Symbol + "," + resourceType + "," + resourcesPerTick + "," + resourcesRemaining);

                writer.Close();
                fileOut.Close();
            }
            catch (Exception fe)
            {
                Debug.WriteLine(fe.Message);
            }
        }
        public override void loadFromFile(string resourceDetails)
        {
            string[] detailsArray = new string[9];
            detailsArray = resourceDetails.Split(',');

            this.x = int.Parse(detailsArray[0]);
            this.y = int.Parse(detailsArray[1]);
            this.health = int.Parse(detailsArray[2]);
            this.faction = detailsArray[3];
            this.symbol = detailsArray[4];
            this.resourceType = detailsArray[5];
            this.resourcesPerTick = int.Parse(detailsArray[6]);
            this.resourcesRemaining = int.Parse(detailsArray[7]);
        }

        public override string toString()
        {
            string output = "X Co-ordinate: " + X + "\n";
            output += "Y Co-ordinate: " + Y + "\n";
            output += "Health: " + Health + "\n";
            output += "Faction: " + Faction + "\n";
            output += "Symbol: " + Symbol + "\n";
            output += "Resource type: " + ResourceType + "\n";
            output += "Resources per second: " + resourcesPerTick + "\n";
            output += "Resources left: " + resourcesRemaining;

            return output;
        }
    }
}
