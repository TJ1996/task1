﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyRTSGame
{
    public partial class FileListDialog : Form
    {
        private string[] fileNamesList;
        private string selectedFile;

        public string SelectedFile
        {
            get { return this.selectedFile; }
        }

        public FileListDialog()
        {
            InitializeComponent();
        }
        public FileListDialog(string[] fileNamesList)
        {
            InitializeComponent();
            this.fileNamesList = fileNamesList;  
            this.lboxSaveGames.Items.AddRange(fileNamesList);
        }

        private void FileListDialog_Load(object sender, EventArgs e)
        {
            
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            DialogResult loadConfirm = new DialogResult();
            loadConfirm = MessageBox.Show("Are you sure you wish to load the game:" + lboxSaveGames.SelectedItem, "Load Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (loadConfirm == DialogResult.Yes)
            {
                this.selectedFile = Convert.ToString(lboxSaveGames.SelectedItem);
                this.Close();
            }            
        }

        private void lboxSaveGames_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            lblSelectedGame.Text = Convert.ToString(lboxSaveGames.SelectedItem);
        }
    }
}
