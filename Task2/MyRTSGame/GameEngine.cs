﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyRTSGame
{
    class GameEngine : Map
    {
        public void updateUnits(Map map1)
        {
            int blueTeamAlive = 0;
            int redTeamAlive = 0;
            List<Unit> unitsToBeRemoved = new List<Unit>();

            foreach(Unit unit in map1.UnitsOnMap)
            {
                if (!unit.isAlive())
                {
                    map1.updatePos(unit.X, unit.Y, 999, 999, "D");
                }

                else
                {
                    Unit closestUnit = unit.nearestUnit(map1.UnitsOnMap);
                    if (unit.isWithinAttackRange(closestUnit))
                        unit.combat(closestUnit);

                    else
                    {
                        map1.move(unit);

                        //if (unit.Attack == false)
                        //    map1.move(unit);
                    }
                }
            }

            foreach (Unit unit in map1.UnitsOnMap)
            {
                if (!unit.isAlive())
                {
                    map1.updatePos(unit.X, unit.Y, 999, 999, "D");
                    unitsToBeRemoved.Add(unit);
                }
                else
                {
                    if (unit.Faction.Equals("Red"))
                    {
                        redTeamAlive++;
                    }
                    if (unit.Faction.Equals("Blue"))
                    {
                        blueTeamAlive++;
                    }
                }
                
            }
            foreach (Unit unit in unitsToBeRemoved)
            {
                map1.UnitsOnMap.Remove(unit);
            }

            if (blueTeamAlive <= 0)
            {
                Form1 gameEnd = new Form1();
                map1.GameState = false;
                gameEnd.gameOver("Red Team");
            }
            else if (redTeamAlive <= 0)
            {
                Form1 gameEnd = new Form1();
                map1.GameState = false;
                gameEnd.gameOver("Blue Team");
            }
            
        }
    }
}
