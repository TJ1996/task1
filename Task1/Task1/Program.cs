﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {

            MeleeUnit barbarian = new MeleeUnit();

            barbarian.Team = "Blue";
            barbarian.Symbol = "Hawk";
            barbarian.YPos = 5;
            barbarian.XPos = 4;
            barbarian.Speed = 4;
            barbarian.Health = 50;
            barbarian.Attack = 5;
            barbarian.AttackRange = 1;

            Console.WriteLine(barbarian.ToString());

        }
    }
}
