﻿using UnityEngine;
using UnityEngine.UI;
public class Play : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(TogglePlay);
    }
    public void TogglePlay()
    {
        Time.timeScale = Mathf.Approximately(Time.timeScale, 0.0f) ? 1.0f : 0.0f;
    }
}