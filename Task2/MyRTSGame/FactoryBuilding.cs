﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace MyRTSGame
{
    class FactoryBuilding : Building
    {
        protected string unitType;
        protected int gameTicksPerUnit;
        protected int spawnX;
        protected int spawnY;

        public FactoryBuilding(int x, int y, int health, string faction, string symbol, string unitType, int gameTicksPerUnit, int spawnX, int spawnY)
        : base(x, y, health, faction, symbol)
        {
            this.unitType = unitType;
            this.gameTicksPerUnit = gameTicksPerUnit;
            this.spawnX = spawnX;
            this.spawnY = spawnY;
        }

        public string UnitType
        {
            get { return this.unitType; }
            set { this.unitType = value; }
        }
        public int GameTicksPerUnit
        {
            get { return this.gameTicksPerUnit; }
            set { this.gameTicksPerUnit = value; }
        }
        public int SpawnX
        {
            get { return spawnX; }
            set { this.spawnX = value; }
        }
        public int SpawnY
        {
            get { return spawnY; }
            set { this.spawnY = value; }
        }

        public void tickProduce(int tickCount, ref string unitType, ref int x, ref int y)
        {
            if ((tickCount % gameTicksPerUnit) == 0)
            {
                unitType = this.unitType;
                x = this.spawnX;
                y = this.spawnY;
            }
            else
                unitType = "";
        }

        public override bool isDestroyed()
        {
            return (health <= 0);
        }

        public override void saveToFile(string fileName)
        {
            FileStream fileOut = null;
            StreamWriter writer = null;

            try
            {
                fileOut = new FileStream(@"Files\" + fileName, FileMode.Append, FileAccess.Write);
                writer = new StreamWriter(fileOut);

                writer.WriteLine(X + "," + Y + "," + Health + "," + Faction + "," + Symbol + "," + UnitType + "," + GameTicksPerUnit + "," + spawnX + "," + spawnY);

                writer.Close();
                fileOut.Close();
            }
            catch (Exception fe)
            {
                Debug.WriteLine(fe.Message);
            }
        }

        public override void loadFromFile(string factoryDetails)
        {
            string[] detailsArray = new string[9];
            detailsArray = factoryDetails.Split(',');

            this.x = int.Parse(detailsArray[0]);
            this.y = int.Parse(detailsArray[1]);
            this.health = int.Parse(detailsArray[2]);
            this.faction = detailsArray[3];
            this.symbol = detailsArray[4];
            this.unitType = detailsArray[5];
            this.gameTicksPerUnit = int.Parse(detailsArray[6]);
            this.spawnX = int.Parse(detailsArray[7]);
            this.spawnY = int.Parse(detailsArray[8]);
        }
        public override string ToString()
        {
            string output = "X Co-ordinate: " + X + "\n";
            output += "Y Co-ordinate: " + Y + "\n";
            output += "Health: " + Health + "\n";
            output += "Faction: " + Faction + "\n";
            output += "Symbol: " + Symbol + "\n";
            output += "Unit spawn type: " + UnitType + "\n";
            output += "Ticks between spawns: " + GameTicksPerUnit + "\n";
            output += "Units spawn at: " + SpawnX + ", " + SpawnY;

            return output;
        }


    }
}
