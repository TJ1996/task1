﻿namespace MyRTSGame
{
    partial class FileListDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lboxSaveGames = new System.Windows.Forms.ListBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.lblSelectedGame = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Previously Saved Games";
            // 
            // lboxSaveGames
            // 
            this.lboxSaveGames.FormattingEnabled = true;
            this.lboxSaveGames.ItemHeight = 16;
            this.lboxSaveGames.Location = new System.Drawing.Point(31, 62);
            this.lboxSaveGames.Name = "lboxSaveGames";
            this.lboxSaveGames.Size = new System.Drawing.Size(373, 164);
            this.lboxSaveGames.TabIndex = 3;
            this.lboxSaveGames.SelectedIndexChanged += new System.EventHandler(this.lboxSaveGames_SelectedIndexChanged_1);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(263, 233);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(141, 32);
            this.btnLoad.TabIndex = 4;
            this.btnLoad.Text = "Load Game";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // lblSelectedGame
            // 
            this.lblSelectedGame.BackColor = System.Drawing.SystemColors.Window;
            this.lblSelectedGame.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSelectedGame.Location = new System.Drawing.Point(31, 233);
            this.lblSelectedGame.Name = "lblSelectedGame";
            this.lblSelectedGame.Size = new System.Drawing.Size(226, 32);
            this.lblSelectedGame.TabIndex = 5;
            // 
            // FileListDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 289);
            this.Controls.Add(this.lblSelectedGame);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.lboxSaveGames);
            this.Controls.Add(this.label1);
            this.Name = "FileListDialog";
            this.Text = "Saved Games";
            this.Load += new System.EventHandler(this.FileListDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lboxSaveGames;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label lblSelectedGame;
    }
}